import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.*;
import java.io.*;

public class Client {

    public static Object getHttpContent(String string) {

        String content="";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();

        } catch (Exception e) {
            System.out.println("Exception error occurred in getHTTPContent");
            System.out.println("\nGoodbye!");
            System.exit(1);
        }

        Teachers teacher2 = JSONToTeacher(content);

        return teacher2;
    }

    public static Teachers JSONToTeacher(String t) {

        ObjectMapper mapper = new ObjectMapper();
        Teachers teacher = null;

        try {
            teacher = mapper.readValue(t, Teachers.class);
        } catch (JsonProcessingException e) {
            System.out.println("ERROR: There is a problem in parsing the JSON");
            System.out.println("\nGoodbye!");
            System.exit(1);
        }

        return teacher;
    }

}


