/*
Rombie Lagunzad
CIT 360
Brother Lawrence

    A. Write an application with two separate components. The first is an HTTP server component that when called
    takes an object and converts it to JSON using the Jackson JSON parser, and returns a block of JSON using HTTP.
    The second component is a client component. It makes a call to the server component, gets the JSON it returns,
    and then converts that using the Jackson JSON parser, into an object.
    B. These two components need to be written as two separate classes.
    C. Make sure you incorporate error handling and data validation into your programs,
    including catching the most specific type of exceptions you can.
    D. Use this sequence diagram to help you understand what is required.

 */

import java.net.InetSocketAddress;
import java.io.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class Week4 {

    public static void main(String[] args) throws Exception {

        try {
            //  Create a server
            HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
            server.createContext("/teacher", new MyHandler());
            server.setExecutor(null); // creates a default executor

            // Start the server
            server.start();
            System.out.println("Server is starting...");

            System.out.println("\n*** FALL 2020 SEMESTER ***");
            Object z = Client.getHttpContent("http://localhost:8080/teacher");
            System.out.println(z.toString());

            System.out.println("\nServer is shutting down...");
            server.stop(3);
            System.out.println("\nGoodbye!");
        }
        catch (Exception e) {
            System.out.println("ERROR: An error occurred in creating the server");
            System.out.println("\nGoodbye!");
            System.exit(1);
        }

    }

    public static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {

            // Create the object
            Teachers instructor = new Teachers();
            instructor.setName("Luke Skywalker");
            instructor.setSubject("History");
            instructor.setMode("Online");

            // Call the teacherToJSON method from the ServerContent class to convert object to JSON
            String jsonConverted = ServerContent.teacherToJSON(instructor);

            try {
            // Send the response to be displayed in http://localhost:8080/teacher
            t.sendResponseHeaders(200, jsonConverted.length());

            OutputStream os = t.getResponseBody();
            os.write(jsonConverted.getBytes());
            os.close();
            }
            catch (IOException e) {
                System.out.println("An Input/Output Exception error occurred in using OutputStream");
                System.out.println("\nGoodbye!");
                System.exit(1);
            }

        }
    }

}