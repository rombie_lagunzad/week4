import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class ServerContent {
    public static String teacherToJSON(Teachers teacher) {

        ObjectMapper mapper = new ObjectMapper();
        String t = "";

        try {
            t = mapper.writeValueAsString(teacher);
        }
        catch (JsonProcessingException e) {
            System.out.println("ERROR: There is a problem in processing the JSON");
            System.out.println("\nGoodbye!");
            System.exit(1);
        }

        return t;
    }

}
